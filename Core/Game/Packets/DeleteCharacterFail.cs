﻿using QuantumCore.Core.Packets;

namespace QuantumCore.Game.Packets
{
    [Packet(0x0B, EDirection.Outgoing)]
    public class DeleteCharacterFail
    {
    }
}
